#!/bin/sh

BOARD_DIR="$(dirname $0)"
BOARD_NAME="$(basename ${BOARD_DIR})"
GENIMAGE_CFG="${BOARD_DIR}/genimage-${BOARD_NAME}.cfg"
GENIMAGE_TMP="${BUILD_DIR}/genimage.tmp"

cp ${BOARD_DIR}/config.txt ${BINARIES_DIR}/config.txt
cp ${BOARD_DIR}/cmdline.txt ${BINARIES_DIR}/cmdline.txt

#v ${BINARIES_DIR}/zImage ${BINARIES_DIR}/kernel.img

rm -rf "${GENIMAGE_TMP}"

genimage                           \
  --rootpath "${TARGET_DIR}"     \
  --tmppath "${GENIMAGE_TMP}"    \
  --inputpath "${BINARIES_DIR}"  \
  --outputpath "${BINARIES_DIR}" \
  --config "${GENIMAGE_CFG}"

exit $?
