#!/bin/sh

set -u
set -e

printf "Your wifi ssid: "
read ssid
printf "\n"
stty -echo
printf "Your wifi psk: "
read psk
stty echo
printf "\n"

sed -i "s|<YOUR_SSID>|$ssid|g" ${TARGET_DIR}/etc/wpa_supplicant.conf
sed -i "s|<YOUR_PSK>|$psk|g" ${TARGET_DIR}/etc/wpa_supplicant.conf

# Add a console on tty1
if [ -e ${TARGET_DIR}/etc/inittab ]; then
    grep -qE '^tty1::' ${TARGET_DIR}/etc/inittab || \
  sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/sbin/getty -L  tty1 0 vt100 # HDMI console' ${TARGET_DIR}/etc/inittab
fi
