################################################################################
#
# python-spi-py
#
################################################################################
PYTHON_SPI_PY_VERSION = 1.0
PYTHON_SPI_PY_SOURCE = master.tar.gz
PYTHON_SPI_PY_SITE = https://github.com/lthiery/SPI-Py/archive
PYTHON_SPI_PY_SETUP_TYPE = distutils

$(eval $(python-package))
