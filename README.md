**Important**

At the moment, only the Raspberry Pi Zero W is supported.

# Prerequisits

You need a copy of [buildroot](https://buildroot.org/download.html) and meet its [system requirements](https://buildroot.org/downloads/manual/manual.html#requirement).

# How to build

In the root directory of buildroot, configure it using the jukky-configuration file for the Raspberry Pi Zero W:

    make BR2_EXTERNAL=/path/to/jukky-buildroot rpi0w_defconfig

Replace `/path/to/jukky-buildroot` with the absolute path to your copy of `jukky-buildroot`.

After that, you can build the image:

    make BR2_EXTERNAL=/path/to/jukky-buildroot

The make process can take a long time...

At the end you will be asked for the SSID and PSK for your WIFI. This will be used to generate a working wpa_supplicant.conf.

**Important**

The file will then exist within the `output/target/etc/` directory containing your settings.

# The Image

The image can be found at `output/images/rpi0w-sdcard.img`.

# Configure

You can change the predefined configuration by using the buildroot configuration menu:

    make BR2_EXTERNAL=/path/to/jukky-buildroot nconfig

